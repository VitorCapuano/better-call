1.3.1 (2019-08-02)
==================

Features
--------

- Save price to charge on database (save_price_bill)


1.3.0 (2019-08-01)
==================

Features
--------

- Self foreign key on phonecall model (self_phonecall_foreign_key)


Bugfixes
--------

- Fix validation to report bill (bill_report_serializer)


Misc
----

- Use environ env to set up pricing (use_environ_to_price)
- Create helper to validate uuid (validate_uuid)


1.1.1 (2019-07-31)
==================

Misc
----

- Fix typo (fix_typo)


1.1.0 (2019-07-31)
==================

Features
--------

- Create get resources to phonecall (create_get_resources_to_phonecall)


1.0.1 (2019-07-31)
==================

Misc
----

- Change report bill url (change_report_url)


1.0.0 (2019-07-31)
==================

Features
--------

- Add crons to run every monday to send reports (crons_final_month)
- Register start and end record to phonecall (phonecall)
- Generate report bill (report_bill)
