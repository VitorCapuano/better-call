# Better Call

[![pipeline status](https://gitlab.com/VitorCapuano/better-call/badges/master/pipeline.svg)](https://gitlab.com/VitorCapuano/better-call/commits/master)

[![coverage report](https://gitlab.com/VitorCapuano/better-call/badges/master/coverage.svg)](https://gitlab.com/VitorCapuano/better-call/commits/master)


Telephone Call's platform
---

Platform that records phone calls.

How to use:
- Send the beginning of each call
- Send end of each call

In the end we generate the rate based report informing the consumer

Stack
---

- Python
- Redis
- Queue
- Schedule Crons

# Tasks
## Tasks completed
- [x] Record the call of calls at the beginning;
- [x] Record the call of calls at the end;
- [x] Get phonecall by id
- [x] Get phonecall list
- [x] Generate PhoneCall Report;
- [x] Send report to a queue or generate response on time (Configurable Flag via environment variable)
- [x] Creates pricing backend to calculate for report;
- [x] Creates storage save backend for report;
- [x] Creates notification save backend for report;
- [x] CronJob to report bill initial of each month
- [x] Already calculated call price can not change

## Tasks to do
- [ ] Deploy admin
- [ ] Behavior-driven development (BDD) testing
- [ ] Support as SAAS application accepting organizations and parametrizing in all API
- [ ] Sets price with each organization
- [ ] Creates writer backend to support (PDF, JSON, etc)
- [ ] Authentication


## Requirements

- Python 3

## Installing

- Is recommend the use of [virtualenv](https://virtualenv.pypa.io/) to manage Python enviroment
- Install the requirements:

    `make requirements-dev`


# Makefile
## Essentials commands:
```
Commmands:           Descriptions:

migrations          Create migrations to database
migrate             Migrate fields to DB
detect-migrations   Detect migrations to be done
run                 Start the API
admin               Start the admin
run-worker          Run worker
```

## Development Options
* `make requirements-dev`<br/>
    * Install all dependencies of project<br/>
* `make run`<br/>
    * Start the application | Go to [localhost:8080](http://localhost:8080/)`<br/>
* `make admin`<br/>
    * Start the admin | Go to [localhost:8000](http://localhost:8000/)`<br/>
* `make run-worker`<br/>
    * Start the worker process <br/>
* `make test`<br/>
    * Run your app's tests<br/>
* `make test-matching Q={string}`<br/>
    * Run specific folder/module/class/function to test.<br/>
* `make flake8`<br/>
    * Check format code of the all project (flake8)<br/>
* `make fix-python-import`<br/>
    * Fix imports order<br/>
* `make shell`<br/>
    * Start shell of application<br/>

# PhoneCall Diagram

## PhoneCall register INITITAL record

[SWAGGER DOCUMENTATION REGISTER START RECORD TO PHONE CALL](https://bettercall.herokuapp.com/docs/#/default/post_phonecall_)
This flow use schema: `StartPhoneCall` (See in swagger)

![](https://www.websequencediagrams.com/cgi-bin/cdraw?lz=dGl0bGUgUGhvbmVDYWxsIFN0YXJ0IAAGBVJlY29yZAoKQ2xpZW50LT5BcGxpY2F0aW9uOgAgBwAtCnJlcXVlc3QKABoKLT5TZXJpYWxpemVyOiBWYWxpZGF0ZSBzb3VyY2UgYW5kIGRlc3RpbgBMBSBmaWVsZHMgYXJlIHZhbGlkIHBob25lIG51bWJlcnMKYWx0IElzAA8Mcy9wYXlsb2FkCiAgICAAYQoAgRkOTW9kZWwgaW5zdGFuY2UgcmVhZHkgdG8gYmUgc2F2ZQA3BgCBKAxEQjogU2F2ZQBRBURCAIFmDwAeFQCCGQY6AII5C3N1Y2Nlc3MgcmV0dXJuIAplbHNlIEluAIENMQCCNwogZXJyb3IAYhlCYWQgUgCCewdlbmQK&s=modern-blue)

## PhoneCall register END record

[SWAGGER DOCUMENTATION REGISTER START RECORD TO END CALL](https://bettercall.herokuapp.com/docs/#/default/post_phonecall_)
This flow use schema: `EndPhoneCall` (See in swagger)

![](https://www.websequencediagrams.com/cgi-bin/cdraw?lz=dGl0bGUgUGhvbmVDYWxsIEVuZCAABAVSZWNvcmQKCkNsaWVudC0-QXBsaWNhdGlvbjoAIAUAKQpyZXF1ZXN0CgAYCi0-U2VyaWFsaXplcjogVmFsaWRhdGUgY2FsbF9pZCB3aXRoIGVuZF9jYWxsIHR5cGUgYWxyZWFkeSBleGlzdHMKADUKLT5EQgA5CgphbHQAgRkGSWQAJAggICAgREIAZQ5UcnVlIHJldHVybgAbBQBEDACBPQxNb2RlbCBpbnN0YW5jZSAAfgZ0byBiZSBzYXZlZABYBQCBTAxEQjogU2F2ZQBtCQCCCwxTAB4VAII7BjoAglkLc3VjY2VzcwCBGgcgCmVsc2UgSW52YWxpAIJRBwCBQBVGYWxzAIExJQCCcwogZXJyb3IAehlCYWQgUgCDNwdlbmQK&s=modern-blue)

## PhoneCall Get ALL

[SWAGGER DOCUMENTATION GET ALL PHONECALLS](https://bettercall.herokuapp.com/docs/#/default/get_phonecall_)

![](https://www.websequencediagrams.com/cgi-bin/cdraw?lz=dGl0bGUgR2V0IGxpc3QKCkNsaWVudC0-QXBsaWNhdGlvbjogUGhvbmUgY2FsbCBnACIHIHJlcXVlc3QKAB4KLT5EQjoARQVhbGwALgZDYWxscwpEQgBBDlJldHVybgBBBm9mAB4MAEEMAIEDBjogUmVuZGVyIGFuZCBwYWdpbmF0ZQBRDA&s=modern-blue)

## PhoneCall Get By ID

[SWAGGER DOCUMENTATION GET By ID PHONECALLS](https://bettercall.herokuapp.com/docs/#/default/get_phonecall__call_id_)

![](https://www.websequencediagrams.com/cgi-bin/cdraw?lz=dGl0bGUgR2V0IGJ5IElECgpDbGllbnQtPkFwbGljYXRpb246IFBob25lIGNhbGwgZwAkBmlkIHJlcXVlc3QKYWx0IFZhbGlkYXRlIHV1aWQKICAgIAA1CgA9DgAlBQBFBgAXE0RCOgCBBwVwaG9uZWNhbGxzIHdpdGggaW5mb3JtZQArDkRCAIEYDlJldHVybiBxdWVyeXNldACBAgVhbHQgRXhpc3RzAGwIIHJlZ2lzdGVyIGluIERCAIEoBQCBIRAAgX4GAEkJcmVzcG9uc2UgMjAwAIEKBgCBFgoAgWgFZWxzZSBOb3QgZQBlBm8AMihOb3RGb3VuZCA0MDQKAEEFRXJyb3IgdgCCSQcAgjoRAIESCEJhZCBSAIJ7BiA0MDAK&s=modern-blue)

# Generate report bill

[SWAGGER DOCUMENTATION TO GENERATE REPORT BILL REQUEST](https://bettercall.herokuapp.com/docs/#/default/post_bill_)
This flow use schema: `period` and `source` fields.(See in swagger)

![](https://www.websequencediagrams.com/cgi-bin/cdraw?lz=dGl0bGUgR2VuZXJhdGUgUmVwb3J0IEJpbGwKCkNsaWVudC0-QXBsaWNhdGlvbjoAIApyACQGYmlsbCByZXF1ZXN0CgAfCi0-U2VyaWFsaXplcjogVmFsaWRhdGUgc291cmNlIGFuZCBwZXJpb2QKYWx0ABYJZAogICAgIAAwCgBtDgBBBSBzAFAIZCBkYXRhAC4FYWx0IENvbmZpZyBmbGFnIHRvIHJlbmRlcgCBHghvbiBodHRwIHJlc3BvbnNlAGMGICAgAIEmDERCOiBHZXQgYWxsIGJpbHMgaW4AgR8HLwCBMgYALQlEQgCCBw5SZXR1cm4gbGlzdCBvZgCCOQVzAE4VAIJMBgAoCQCBFApqc29uIChzeW5jIGcAglQOKQCCEAVlbHNlAIFTEGVucXVldQCDAggAgT4VUXVldWU6IEUAIwd0YXNrIHRvAFMQAIEHJHN1Y2Nlc3Mgbm90aWZjYXRpbmcAg3sId2lsbCBiZSBzZW5kIHNvb24gLSBSAIJiBzogMjAwAIJlCWFsdCBCYWNrZ3JvdW5kIHdvcmtlcgCDAwkAgSgFAIRgDkNvbnN1bQCBMwYAgjx4AIVyDFNhdmUgb24gYmFjawCBaQV0b3JhZwCEORYAhioMTgCCLwdlAIRYBWFjdGl2ZXMAPghzIHdpdGggc2lnbmVkX3VybACFFQkKAINyBUludgCFdQUAhkkJIEQAhXEIAIYTGACGcwogZXJybwCCXgYAhHUUQmFkIFIAhzcHZW5kCg&s=modern-blue)


### Environment variables

- DEBUG
- DATABASES_DEFAULT_URI
- DEFAULT_PRICING_CALL_CHARGE
- DEFAULT_PRICING_TARIFF
- DEFAULT_PRICING_BACKEND
- DEFAULT_NOTIFICATION_BACKEND
- DEFAULT_STORAGE_BACKEND
- IS_ENABLE_TO_ENQUEUE_REPORT_BILL


## Twelve-factor

[_Twelve-factor app: A methodology for building modern, scalable, maintainable software-as-a-service apps._](https://12factor.net/)

- [x] I - Codebase: One codebase tracked in revision control, many deploys
- [x] II - Dependencies: Explicitly declare and isolate dependencies
- [X] III - Config: Store config in the environment
- [X] IV - Backing services: Treat backing services as attached resources
- [x] V - Build, release, run: Strictly separate build and run stages
- [X] VI - Processes: Execute the app as one or more stateless processes
- [X] VII - Port binding: Export services via port binding
- [] VIII - Concurrency: Scale out via the process model
- [x] IX - Disposability: Maximize robustness with fast startup and graceful shutdown
- [X] X - Dev/prod parity: Keep development, staging, and production as similar as possible
- [x] XI - Logs: Treat logs as event streams
- [x] XII - Admin processes: Run admin/management tasks as one-off processes

