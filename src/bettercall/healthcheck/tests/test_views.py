from http import HTTPStatus

import pytest


class TestPingView:
    @pytest.fixture
    def route(self):
        return '/ping/'

    async def test_ping_should_return_success(self, client, route):
        response = await client.get(route)
        assert response.status == HTTPStatus.OK
