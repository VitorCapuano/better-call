from .views import PingView

HEALTHCHECK_ROUTES = [
    ('*', '/ping/', PingView, 'ping')
]
