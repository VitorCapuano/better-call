from aiohttp import web

from bettercall.version import __version__


class PingView(web.View):

    async def get(self):
        return web.json_response(
            {'ok': True, 'version': __version__}
        )
