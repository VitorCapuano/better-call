
from rq import Connection, Queue, Worker
from simple_settings import settings

listen = ['high', 'default', 'low']

if __name__ == '__main__':
    with Connection(settings.CONN):
        worker = Worker(map(Queue, listen))
        worker.work()
