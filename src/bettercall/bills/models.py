from dataclasses import dataclass
from datetime import date, datetime
from decimal import Decimal

from bettercall.settings.constants import (
    DATE_FORMAT_OUTPUT,
    DATETIME_FORMAT_OUTPUT,
    MINUTES
)


@dataclass(frozen=True)
class PhoneCallBill:
    destination: str
    start_time: datetime
    end_time: datetime
    price: Decimal

    @property
    def duration(self) -> datetime:
        return self.end_time - self.start_time

    @property
    def start_date(self) -> date:
        return self.start_time.date()

    def as_dict(self):
        return {
            'destination': self.destination,
            'start_time': self.start_time.strftime(DATETIME_FORMAT_OUTPUT),
            'end_time': self.end_time.strftime(DATETIME_FORMAT_OUTPUT),
            'price': float(self.price),
            'duration': int(self.duration.total_seconds() / MINUTES),
            'start_date': self.start_date.strftime(DATE_FORMAT_OUTPUT)
        }
