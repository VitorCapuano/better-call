import logging
from datetime import datetime

from simple_settings import settings

from bettercall.backends.notification.backend import NotificationBackendError
from bettercall.backends.pools.notification import NotificationBackendPool
from bettercall.backends.pools.storage import StorageBackendPool
from bettercall.backends.storage.backend import StorageBackendError
from bettercall.helpers.date import get_previous_month
from bettercall.phonecall.models import PhoneCall
from bettercall.settings.constants import DATE_FORMAT_OUTPUT

logger = logging.getLogger(__name__)


class GenerateReportBillError(Exception):
    pass


async def generate_report_bill(date: datetime, source: str):
    logger.info(
        f'Get report bill from date: {date.strftime(DATE_FORMAT_OUTPUT)}.'
    )
    storage_backend = StorageBackendPool.get(
        settings.DEFAULT_NOTIFICATION_BACKEND
    )
    try:
        report = PhoneCall.generate_report_bill(date=date, source=source)
        signed_url = await storage_backend.save(report, date)
        for backend in NotificationBackendPool.actives():
            await backend.notify_phonecall_bill(
                signed_url=signed_url
            )
    except NotificationBackendError as notify_ex:
        logger.error(f'Error on notify report bill: {notify_ex}.')
        raise NotificationBackendError() from notify_ex
    except StorageBackendError as storage_ex:
        logger.error(f'Error on save report in storage: {storage_ex}.')
        raise StorageBackendError() from storage_ex
    except Exception as ex:
        logger.error(f'Error on generate report bill task: {ex}.')
        raise GenerateReportBillError() from ex


async def report_to_all_sources():
    for related in PhoneCall.objects.values('source').distinct():
        await generate_report_bill(
            date=get_previous_month(),
            source=related['source']
        )
