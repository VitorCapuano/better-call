from datetime import datetime

from marshmallow import Schema, ValidationError, fields, validate, validates

from bettercall.helpers.date import get_previous_month
from bettercall.helpers.regex import DATE_MATCH_REGEX, PHONE_MATCH_REGEX
from bettercall.helpers.serializer import HandleErrorMixin


class PhoneBillSerializer(HandleErrorMixin, Schema):
    source = fields.String(
        validate=validate.Regexp(PHONE_MATCH_REGEX),
        required=True
    )
    period = fields.String(
        validate=[
            validate.Regexp(
                DATE_MATCH_REGEX,
                error='Date not match with format mm/yyyy'
            ),
            validate.Length(min=7, max=7)
        ],
        missing=get_previous_month().strftime('%m/%Y')
    )

    @validates('period')
    def validate_period(self, data, **kwargs):
        if self._is_period_greater_than_now(data):
            raise ValidationError('Period bigger than now not allowed')

    def _is_period_greater_than_now(self, period):
        return datetime.strptime(
            period, '%m/%Y'
        ).date() > get_previous_month()

    class Meta:
        ordered = True
