import logging
from datetime import datetime
from http import HTTPStatus

from aiohttp import web
from rq import Queue
from simple_settings import settings

from bettercall.phonecall.models import PhoneCall
from bettercall.settings.constants import DATE_MONTH_OUTPUT

from .serializers import PhoneBillSerializer
from .tasks import generate_report_bill

logger = logging.getLogger(__name__)


class PhoneBillView(web.View):

    async def post(self):
        body = await self.request.json()
        logger.info(f'Request to get phonecall bill with data: {body}.')
        data = PhoneBillSerializer(strict=True).load(body).data
        date = datetime.strptime(data['period'], DATE_MONTH_OUTPUT)

        if settings.IS_ENABLE_TO_ENQUEUE_REPORT_BILL:
            await self.__enqueue_generate_report(
                date=date,
                source=data['source']
            )
            return_message = (
                f'Report will be send on notification to: {data["source"]}'
            )

            logger.info(return_message)
            return_data = {'data': return_message}
        else:
            report = PhoneCall.generate_report_bill(
                date=date,
                source=data['source']
            )

            logger.info(f'Processed report to render on time. Data: {data}')
            return_data = {'data': [bill.as_dict() for bill in report]}

        return web.json_response(
            return_data,
            status=HTTPStatus.OK.value
        )

    async def __enqueue_generate_report(self, date, source):
        q = Queue('high', connection=settings.CONN)
        q.enqueue(
            generate_report_bill,
            **{
                'date': date,
                'source': source
            }
        )
