import pytest
from dateutil.relativedelta import relativedelta
from django.utils import timezone

from bettercall.bills.serializers import PhoneBillSerializer
from bettercall.exceptions.api import ValidationError
from bettercall.helpers.date import get_previous_month


class TestPhoneBillCallbackSerializer:

    @pytest.fixture
    def bill_payload(self):
        return {
            'source': '11900001111',
            'period': '09/2018'
        }

    @pytest.mark.parametrize('invalid_phone', [
        '99',
        '99',
        '999',
        '9999',
        '99999',
        '199999',
        '1199999',
        '11999999',
    ])
    def test_should_raise_validation_error_to_invalid_source_phone(
        self,
        bill_payload,
        invalid_phone
    ):
        bill_payload['source'] = invalid_phone

        with pytest.raises(ValidationError):
            PhoneBillSerializer(strict=True).load(
                bill_payload
            ).data

    @pytest.mark.parametrize('invalid_format_date', [
        '01/20',
        '01/01/2009',
    ])
    def test_should_raise_validation_error_to_invalid_length_to_period(
        self,
        bill_payload,
        invalid_format_date
    ):
        bill_payload['period'] = invalid_format_date

        with pytest.raises(ValidationError):
            PhoneBillSerializer(strict=True).load(
                bill_payload
            ).data

    @pytest.mark.parametrize('invalid_date', [
        '99/2030',
        '2009/11',
    ])
    def test_should_raise_validation_error_to_not_match_date_to_period(
        self,
        bill_payload,
        invalid_date
    ):
        bill_payload['period'] = invalid_date

        with pytest.raises(ValidationError) as e:
            PhoneBillSerializer(strict=True).load(
                bill_payload
            ).data

        assert (
            e.value.details['period'][0] ==
            'Date not match with format mm/yyyy'
        )

    def test_should_get_bill_payload(self, bill_payload):
        serializer = PhoneBillSerializer(strict=True).load(
            bill_payload
        ).data

        assert serializer['source'] == bill_payload['source']
        assert serializer['period'] == bill_payload['period']

    def test_should_get_previous_month_without_required_period(
        self,
        bill_payload
    ):
        del bill_payload['period']

        serializer = PhoneBillSerializer(strict=True).load(
            bill_payload
        ).data

        assert serializer['source'] == bill_payload['source']
        assert serializer['period'] == get_previous_month().strftime('%m/%Y')

    @pytest.mark.parametrize('date_bigger_than_now', [
        timezone.now().strftime('%m/%Y'),
        (timezone.now() + relativedelta(months=1)).strftime('%m/%Y'),
        (timezone.now() + relativedelta(months=2)).strftime('%m/%Y')
    ])
    def test_should_raise_an_validate_exception_to_dates_greater_than_now(
        self,
        bill_payload,
        date_bigger_than_now
    ):
        bill_payload['period'] = date_bigger_than_now

        with pytest.raises(ValidationError) as e:
            PhoneBillSerializer(strict=True).load(
                bill_payload
            ).data

        assert (
            e.value.details['period'][0] ==
            'Period bigger than now not allowed'
        )
