from datetime import datetime
from unittest import mock

import pytest

from bettercall.backends.storage.backend import StorageBackendError
from bettercall.bills.tasks import (
    GenerateReportBillError,
    generate_report_bill
)


@pytest.mark.django_db
class TestReportTask:

    @pytest.fixture
    def date(self):
        return datetime.now()

    @pytest.fixture
    def source(self):
        return '11900011111'

    async def test_generate_report_bill_should_call_model_function(
        self,
        date,
        source
    ):
        with mock.patch('bettercall.bills.tasks.PhoneCall') as mock_phone_call:
            await generate_report_bill(date, source)
            mock_generate_report = mock_phone_call.generate_report_bill

        mock_generate_report.awaited_once_with(date=date)

    async def test_generate_report_bill_should_raise_expected_to_save_storage(
        self,
        date,
        source
    ):
        with mock.patch(
            'bettercall.bills.tasks.StorageBackendPool'
        ) as mock_backend_pool:
            mock_backend = mock_backend_pool.get.return_value
            mock_backend.save.side_effect = StorageBackendError()
            with pytest.raises(StorageBackendError):
                await generate_report_bill(date, source)

        assert mock_backend.awaited

    async def test_generate_report_bill_should_raise_expected_exception(
        self,
        date,
        source
    ):
        with mock.patch('bettercall.bills.tasks.PhoneCall') as mock_phone_call:
            mock_phone_call.generate_report_bill.side_effect = Exception()
            with pytest.raises(GenerateReportBillError):
                await generate_report_bill(date, source)

    async def test_should_not_raise_error(self, date, source):
        try:
            await generate_report_bill(date, source)
        except Exception as e:
            pytest.fail(f'It should not have raised: {e}')
