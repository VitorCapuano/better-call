from datetime import datetime

import pytest
from asynctest import mock
from model_mommy import mommy

from bettercall.bills.tasks import report_to_all_sources
from bettercall.helpers.date import make_aware_date


@pytest.mark.django_db
class TestReportTask:

    @pytest.fixture(autouse=True)
    def start_and_end_call(self):
        mommy.make(
            'phonecall.PhoneCall',
            _record_type='call_started',
            timestamp=make_aware_date(
                datetime(year=2018, month=9, hour=1, day=1)
            ),
            source='11900001111',
            destination='111999',
            call_id='eda63214-279b-422d-8403-ba1cc40e08fb'
        )
        mommy.make(
            'phonecall.PhoneCall',
            _record_type='call_ended',
            timestamp=make_aware_date(
                datetime(year=2018, month=9, hour=3, day=1)
            ),
            source='11900001111',
            destination='111999',
            call_id='eda63214-279b-422d-8403-ba1cc40e08fb'
        )
        return

    async def test_should_call_with_previous_month(self):
        with mock.patch(
            'bettercall.bills.tasks.generate_report_bill',
        ) as mock_generate_report:
            await report_to_all_sources()

        assert mock_generate_report.awaited

    async def test_should_not_raise_error(self):
        try:
            await report_to_all_sources()
        except Exception as e:
            pytest.fail(f'It should not have raised: {e}')
