from datetime import datetime
from http import HTTPStatus

import pytest
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from model_mommy import mommy
from simple_settings import settings

from bettercall.helpers.date import make_aware_date


@pytest.mark.django_db
class TestGetPhoneBillView:

    @pytest.fixture
    def url(self, app):
        return app.router['post-phone-bill'].url_for()

    @pytest.fixture
    def invalid_paylod(self):
        return {}

    @pytest.fixture
    def valid_payload(self):
        return {
            'period': '09/2018',
            'source': '11900001111'
        }

    @pytest.fixture(autouse=True)
    def start_and_end_call(self):
        mommy.make(
            'phonecall.PhoneCall',
            _record_type='call_started',
            timestamp=make_aware_date(
                datetime(year=2018, month=9, hour=1, day=1)
            ),
            source='11900001111',
            destination='111999',
            call_id='eda63214-279b-422d-8403-ba1cc40e08fb'
        )
        mommy.make(
            'phonecall.PhoneCall',
            _record_type='call_ended',
            timestamp=make_aware_date(
                datetime(year=2018, month=9, hour=3, day=1)
            ),
            source='11900001111',
            destination='111999',
            call_id='eda63214-279b-422d-8403-ba1cc40e08fb'
        )
        return

    async def test_should_raise_validation_error_empty_payload(
        self,
        client,
        url,
        invalid_paylod
    ):
        response = await client.post(path=url, json=invalid_paylod)
        data = await response.json()

        assert response.status == HTTPStatus.BAD_REQUEST.value
        assert data['message'] == 'Invalid input.'

    async def test_should_raise_bad_request_without_source(
        self,
        client,
        url,
        valid_payload
    ):
        del valid_payload['source']

        response = await client.post(path=url, json=valid_payload)
        data = await response.json()

        assert response.status == HTTPStatus.BAD_REQUEST.value
        assert data['message'] == 'Invalid input.'

    @pytest.mark.parametrize('date_bigger_than_now', [
        timezone.now().strftime('%m/%Y'),
        (timezone.now() + relativedelta(months=1)).strftime('%m/%Y'),
        (timezone.now() + relativedelta(months=2)).strftime('%m/%Y')
    ])
    async def test_should_raise_bad_request_with_period_bigger_now(
        self,
        client,
        url,
        date_bigger_than_now,
        valid_payload
    ):
        valid_payload['period'] = date_bigger_than_now

        response = await client.post(path=url, json=valid_payload)
        data = await response.json()

        assert response.status == HTTPStatus.BAD_REQUEST.value
        assert data['message'] == 'Invalid input.'

    async def test_should_return_ok_status_code_to_bill_payload(
        self,
        client,
        url,
        valid_payload
    ):
        response = await client.post(path=url, json=valid_payload)
        assert response.status == HTTPStatus.OK.value

    async def test_should_return_report_payload_to_bills(
        self,
        client,
        url,
        valid_payload
    ):
        response = await client.post(path=url, json=valid_payload)
        json = await response.json()

        assert response.status == HTTPStatus.OK.value
        assert isinstance(json['data'], list)

    async def test_should_send_to_queue_with_settings_to_enqueue_active(
        self,
        client,
        url,
        valid_payload
    ):
        settings.IS_ENABLE_TO_ENQUEUE_REPORT_BILL = True

        response = await client.post(path=url, json=valid_payload)
        json = await response.json()

        assert response.status == HTTPStatus.OK.value
        assert json['data'] == (
            f'Report will be send on notification to: '
            f'{valid_payload["source"]}'
        )
