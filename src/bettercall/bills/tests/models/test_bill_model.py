from datetime import datetime
from decimal import Decimal

import pytest

from bettercall.bills.models import PhoneCallBill


class TestPhoneCallBill:

    @pytest.fixture
    def model(self):
        return PhoneCallBill(
            destination='119999',
            start_time=datetime(
                year=2019,
                month=1,
                day=1,
                hour=1,
                minute=10,
                second=1
            ),
            end_time=datetime(
                year=2019,
                month=1,
                day=1,
                hour=1,
                minute=30,
                second=1
            ),
            price=Decimal('10.00')
        )

    def test_should_get_start_date_on_property(self, model):
        assert model.start_time.date() == model.start_date

    def test_should_get_duration_of_call_on_property(self, model):
        assert (model.end_time - model.start_time) == model.duration

    def test_should_render_as_dict(self, model):
        assert isinstance(model.as_dict(), dict)
