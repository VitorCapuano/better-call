from .views import PhoneBillView

BILLS_ROUTES = [
    ('POST', '/phonecall/report/', PhoneBillView, 'post-phone-bill')
]
