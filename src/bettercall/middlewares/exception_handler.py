import logging

from aiohttp import web
from aiohttp.web import middleware
from aiohttp.web_exceptions import HTTPError

from bettercall.exceptions.api import APIException
from bettercall.exceptions.mixins import ErrorDetail

logger = logging.getLogger(__name__)


@middleware
async def exception_handler_middleware(request, handler):
    try:
        return (await handler(request))
    except (APIException) as exc:
        payload = exc.get_full_details()
        status_code = exc.status_code
    except HTTPError as exc:
        status_code = exc.status_code
        payload = ErrorDetail(
            code='generic_error',
            message=exc.reason,
            details=None
        )._asdict()
    except Exception as exc:
        api_exception = APIException()
        payload = api_exception.get_full_details()
        status_code = api_exception.status_code
        logger.critical(
            f'Exception: {exc}; '
            f'Name: {exc.__class__.__name__}'
        )

    return web.json_response(
        payload,
        status=status_code
    )
