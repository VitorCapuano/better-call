import json
from http import HTTPStatus

from aiohttp.test_utils import make_mocked_request
from aiohttp.web_exceptions import HTTPNotFound

from bettercall.exceptions.api import ValidationError
from bettercall.middlewares.exception_handler import (
    exception_handler_middleware
)


async def handler_raise_exception(request):
    raise Exception()


async def handler_raise_api_exception_with_bad_request(request):
    raise ValidationError()


async def handler_raise_aiohttp_not_found_exception(request):
    raise HTTPNotFound()


class TestExceptionHandlerMiddleware:

    async def test_should_return_response_with_internal_error(self):
        response = await exception_handler_middleware(
            request=make_mocked_request('GET', '/internal-error'),
            handler=handler_raise_exception
        )

        assert response.status == int(HTTPStatus.INTERNAL_SERVER_ERROR)
        assert response.text == json.dumps({
            'code': 'server_error',
            'message': 'A server error occured.',
            'details': None
        })
        assert response.content_type == 'application/json'

    async def test_should_change_http_status_code_based_on_api_exception(self):
        response = await exception_handler_middleware(
            request=make_mocked_request('GET', '/bad-request'),
            handler=handler_raise_api_exception_with_bad_request
        )

        assert response.status == int(HTTPStatus.BAD_REQUEST)

    async def test_should_return_response_with_validation_error_details(self):
        response = await exception_handler_middleware(
            request=make_mocked_request('GET', '/validation-error'),
            handler=handler_raise_api_exception_with_bad_request
        )
        validation_error = ValidationError()

        assert response.text == json.dumps(validation_error.get_full_details())

    async def test_should_capture_aiohttp_http_error_exceptions(self):
        response = await exception_handler_middleware(
            request=make_mocked_request('GET', '/not-found'),
            handler=handler_raise_aiohttp_not_found_exception
        )

        assert response.status == int(HTTPStatus.NOT_FOUND)
        assert response.text == '{"code": "generic_error", "message": "Not Found", "details": null}'  # noqa
