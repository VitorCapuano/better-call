from http import HTTPStatus

from .mixins import GetErrorDetailsMixin


class APIException(GetErrorDetailsMixin, Exception):
    status_code = int(HTTPStatus.INTERNAL_SERVER_ERROR)
    code = 'server_error'
    message = 'A server error occured.'
    details = None

    def __init__(self, code=None, message=None, details=None):
        if code:
            self.code = code

        if message:
            self.message = message

        if details:
            self.details = details

        super().__init__(self.code, self.message, self.details)


class ValidationError(APIException):
    status_code = int(HTTPStatus.BAD_REQUEST)
    code = 'invalid'
    message = 'Invalid input.'


class NotFound(APIException):
    status_code = int(HTTPStatus.NOT_FOUND)
    code = 'not_found'
    message = 'Not found.'
