from collections import namedtuple

ErrorDetail = namedtuple('ErrorDetail', ['code', 'message', 'details'])


class GetErrorDetailsMixin:

    def get_full_details(self):
        return ErrorDetail(
            code=self.code,
            message=self.message,
            details=self.details
        )._asdict()
