from django.contrib import admin
from django.http import HttpResponse
from django.urls import path

admin.autodiscover()

urlpatterns = [
    path('', admin.site.urls),
    path('healthcheck/', lambda request: HttpResponse('Ok')),
]
