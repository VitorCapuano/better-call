from collections import OrderedDict

from django.core.paginator import Page
from yarl import URL


def build_paginated_response(
    url: URL,
    page: Page,
    serialized_data: list,
) -> dict:

    next_page = None
    previous = None

    if page.has_next():
        next_page = str(url.update_query(page=page.next_page_number()))

    if page.has_previous():
        previous = str(url.update_query(page=page.previous_page_number()))

    return OrderedDict([
        ('count', page.paginator.count),
        ('next', next_page),
        ('previous', previous),
        ('results', serialized_data)
    ])
