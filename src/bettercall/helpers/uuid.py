from uuid import UUID


class ValidateUUIDError(Exception):
    pass


def validate(uuid):
    try:
        UUID(uuid)
    except ValueError:
        raise ValidateUUIDError()
