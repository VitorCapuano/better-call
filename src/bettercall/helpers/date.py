from datetime import datetime

from dateutil.relativedelta import relativedelta
from django.utils import timezone


def get_previous_month(date: datetime = timezone.now()) -> datetime:
    return (date - relativedelta(months=1)).date()


def make_aware_date(timestamp: datetime) -> datetime:
    return (
        timestamp if not timezone.is_naive(timestamp) else
        timezone.make_aware(timestamp)
    )
