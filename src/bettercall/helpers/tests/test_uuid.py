from uuid import uuid4

import pytest

from bettercall.helpers import uuid


class TestValidateUUID:

    def test_should_not_raise_exception_to_valid_uuid(self):
        try:
            uuid.validate(str(uuid4()))
        except Exception as e:
            pytest.fail(f'It should not have raised: {e}')

    @pytest.mark.parametrize('invalid_uuid', [
        '1',
        'not-uuid-0000',
        'ok-not-uuid'
    ])
    def test_should_raise_an_exception_to_not_valid_uuid(self, invalid_uuid):
        with pytest.raises(uuid.ValidateUUIDError):
            uuid.validate(invalid_uuid)
