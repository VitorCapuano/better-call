from datetime import date, datetime

import pytest
from dateutil.relativedelta import relativedelta
from django.utils import timezone

from bettercall.helpers.date import get_previous_month, make_aware_date


class TestPreviousMonth:

    @pytest.fixture
    def now_date(self):
        return timezone.now()

    def test_should_return_an_date_instance(self, now_date):
        assert isinstance(get_previous_month(date=now_date), date)

    def test_should_return_previous_month(self, now_date):
        assert get_previous_month(
            date=now_date
        ) == (now_date - relativedelta(months=1)).date()


class TestAwareDate:

    @pytest.fixture
    def naive_date(self):
        return timezone.now().replace(tzinfo=None)

    @pytest.fixture
    def aware_date(self):
        return timezone.now()

    def test_should_return_an_datetime_to_naive_instance(self, naive_date):
        assert isinstance(make_aware_date(timestamp=naive_date), datetime)

    def test_should_return_an_datetime_to_aware_instance(self, aware_date):
        assert isinstance(make_aware_date(timestamp=aware_date), datetime)

    def test_should_convert_same_datetime_to_aware_from_naive_date(
        self,
        naive_date
    ):
        assert timezone.is_naive(naive_date)
        converted_date = make_aware_date(timestamp=naive_date)

        assert timezone.is_aware(converted_date)
        assert converted_date.strftime(
            '%d/%m/%Y %H:%M:%S'
        ) == naive_date.strftime('%d/%m/%Y %H:%M:%S')

    def test_should_mantain_same_aware_date(self, aware_date):
        assert make_aware_date(timestamp=aware_date) == aware_date
