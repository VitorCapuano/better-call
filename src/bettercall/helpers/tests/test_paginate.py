import pytest
from django.core.paginator import Paginator
from yarl import URL

from bettercall.helpers.pagination import build_paginated_response


class RequestStub:
    query = None
    url = None


@pytest.fixture
def items():
    return [
        {'item': 1},
        {'item': 2},
        {'item': 3}
    ]


@pytest.fixture
def url():
    return URL('http://localhost/?a=1&b=2&c=3')


def test_shoud_build_paginated_response_successfully(items, url):
    paginator = Paginator(items, 1)
    page = paginator.get_page(2)

    response = build_paginated_response(
        url=url,
        page=page,
        serialized_data=page.object_list
    )

    assert response['next'] == 'http://localhost/?a=1&b=2&c=3&page=3'
    assert response['previous'] == 'http://localhost/?a=1&b=2&c=3&page=1'
    assert response['results'] == [items[1]]


def test_should_build_paginated_response_empty_page(items, url):
    paginator = Paginator(items, 1)
    page = paginator.get_page(5)

    response = build_paginated_response(
        url=url,
        page=page,
        serialized_data=page.object_list
    )

    assert response['next'] is None
    assert response['previous'] == 'http://localhost/?a=1&b=2&c=3&page=2'
    assert response['results'] == [items[2]]
