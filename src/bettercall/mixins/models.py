from bettercall.phonecall.exceptions import InvalidRecordType


class ProtectedRecordTypeMixin:

    @property
    def record_type(self):
        return self._record_type

    @record_type.setter
    def record_type(self, value):
        self._record_type = value

    @staticmethod
    def validate_status(record_type, valid_record_types):
        if not (record_type in valid_record_types):
            raise InvalidRecordType('You must use a valid record type')
