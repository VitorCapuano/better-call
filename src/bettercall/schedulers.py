from apscheduler.schedulers.asyncio import AsyncIOScheduler

from bettercall.bills.tasks import report_to_all_sources

scheduler = AsyncIOScheduler()
scheduler.add_job(report_to_all_sources, 'cron', day='1st mon')

scheduler.start()
