import django  # isort:skip
django.setup()

from .factory import build_app  # noqa
from .version import __version__  # noqa

app = build_app()
