import abc


class StorageBackendError(Exception):
    pass


class StorageBackend(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    async def save(self, data, filename):
        pass
