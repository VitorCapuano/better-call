import inspect

from bettercall.backends.pricing.backend import PricingBackend


class TestPricingBackend:

    def test_should_be_abstract_class(self):
        assert inspect.isabstract(PricingBackend)

    def test_property_standard_tariff_should_exist(self):
        assert hasattr(PricingBackend, 'standard_tariff')

    def test_property_standard_call_charge_should_exist(self):
        assert hasattr(PricingBackend, 'standard_call_charge')

    def test_method_calculate_should_exist(self):
        assert hasattr(PricingBackend, 'calculate')

    def test_method_calculate_discount_should_exist(self):
        assert hasattr(PricingBackend, '_calculate_discount_call_charge')
