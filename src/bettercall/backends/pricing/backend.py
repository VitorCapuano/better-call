import abc
from datetime import datetime
from decimal import Decimal
from typing import ClassVar, Optional


class PricingBackendError(Exception):
    pass


class PricingBackend(metaclass=abc.ABCMeta):
    id: ClassVar[Optional[str]] = None
    name: ClassVar[Optional[str]] = None

    @classmethod
    @abc.abstractproperty
    def standard_tariff(cls) -> Decimal:
        """
        Standard tariff
        """
        pass

    @classmethod
    @abc.abstractproperty
    def standard_call_charge(cls) -> Decimal:
        """
        Call charge tariff
        """
        pass

    @classmethod
    @abc.abstractproperty
    def charge_range(cls) -> range:
        """
        Call charge tariff
        """
        pass

    @classmethod
    @abc.abstractmethod
    def calculate(
        cls,
        initial_date: datetime,
        final_date: datetime
    ) -> Decimal:
        """
        Calculates the price of the charge given the dates of the call
        """
        pass

    @classmethod
    @abc.abstractmethod
    def _calculate_discount_call_charge(
        cls,
        initial_date: datetime,
        final_date: datetime
    ) -> int:
        """
        Calculates the price of the discount in minutes
        """
        pass
