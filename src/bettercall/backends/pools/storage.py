from ramos.pool import BackendPool


class StorageBackendPool(BackendPool):
    backend_type = 'storage'
