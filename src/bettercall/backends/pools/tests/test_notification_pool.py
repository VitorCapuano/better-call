from ramos.compat import import_string
from simple_settings import settings

from bettercall.backends.pools.notification import NotificationBackendPool


class TestNotificationBackendPool:

    def test_should_return_same_actives_backends(self):
        backends = NotificationBackendPool.actives()

        assert len(backends) == len(settings.ACTIVE_NOTIFICATION_BACKENDS)

        for backend, active in zip(
            backends,
            settings.ACTIVE_NOTIFICATION_BACKENDS
        ):
            assert backend.__class__ == import_string(active)
