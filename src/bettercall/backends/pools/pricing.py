from ramos.pool import BackendPool


class PricingBackendPool(BackendPool):
    backend_type = 'pricing'
