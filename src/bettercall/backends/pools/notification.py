from django.core.exceptions import ImproperlyConfigured
from ramos.pool import BackendPool
from simple_settings import settings


class NotificationBackendPool(BackendPool):
    backend_type = 'notification'

    @classmethod
    def actives(cls):
        return [
            backend_class.create()
            for backend_class in cls._get_active_backends_classes()
        ]

    @classmethod
    def _get_active_backends_classes(cls):
        try:
            backend_list = settings.ACTIVE_NOTIFICATION_BACKENDS
        except KeyError:
            raise ImproperlyConfigured(
                u'Active backend type "{}" config not found'.format(
                    cls.backend_type
                )
            )

        return [
            cls._get_backend_class(backend_path)
            for backend_path in backend_list
        ]
