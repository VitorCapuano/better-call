import inspect

from bettercall.backends.notification.backend import NotificationBackend


class TestNotificationBackend:

    def test_should_be_abstract_class(self):
        assert inspect.isabstract(NotificationBackend)

    def test_method_notify_phonecall_bill_should_exist(self):
        assert hasattr(NotificationBackend, 'notify_phonecall_bill')
