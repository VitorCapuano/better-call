import abc
from typing import ClassVar, Optional


class NotificationBackendError(Exception):
    pass


class NotificationBackend(metaclass=abc.ABCMeta):
    id: ClassVar[Optional[str]] = None
    name: ClassVar[Optional[str]] = None

    @classmethod
    @abc.abstractmethod
    def notify_phonecall_bill(cls, signed_url: str) -> None:
        """
        Notify by implemented choice to destination
        """
        pass
