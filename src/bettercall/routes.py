from aiohttp_jinja2 import render_template
from simple_settings import settings

from bettercall.bills.routes import BILLS_ROUTES
from bettercall.healthcheck.routes import HEALTHCHECK_ROUTES
from bettercall.phonecall.routes import PHONECALL_ROUTES

ROUTES = []

ROUTES += HEALTHCHECK_ROUTES
ROUTES += PHONECALL_ROUTES
ROUTES += BILLS_ROUTES


async def docs(request):
    response = render_template(
        'docs/index.html',
        request,
        None
    )
    return response


async def swagger(request):
    response = render_template(
        'docs/bettercall.yaml',
        request,
        None
    )
    response.content_type = 'text/yaml'
    return response


def setup_routes(app):
    for http_method, endpoint, handler, name in ROUTES:
        app.router.add_route(
            method=http_method,
            path=endpoint,
            handler=handler,
            name=name
        )

    app.router.add_static('/static/', settings.PROJECT_PATH + '/docs/')

    app.router.add_route(
        method='GET',
        path='/docs/',
        handler=docs,
        name='docs',
    )

    app.router.add_route(
        method='GET',
        path='/docs/bettercall/',
        handler=swagger,
        name='bettercall-swagger-yaml',
    )
