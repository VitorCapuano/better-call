from datetime import datetime
from decimal import Decimal
from unittest import mock

import pytest

from bettercall.backends.pools.pricing import PricingBackendPool
from bettercall.backends.pricing.backend import PricingBackendError
from bettercall.extensions.default_pricing.backend import DefaultPricingBackend


class TestDefaultPricingBackend:

    @pytest.fixture
    def backend(self):
        return DefaultPricingBackend.create()

    def test_should_get_backend_from_pool(self):
        assert isinstance(
            PricingBackendPool.get(DefaultPricingBackend.id),
            DefaultPricingBackend
        )

    def test_should_return_all_tariffs(self, backend):
        assert isinstance(backend.standard_tariff, Decimal)
        assert isinstance(backend.standard_call_charge, Decimal)
        assert isinstance(backend.charge_range, range)

    def test_should_raise_a_backend_exception_to_base_exception(self, backend):
        with mock.patch.object(
            DefaultPricingBackend,
            '_calculate_discount_call_charge',
            side_effect=Exception()
        ):
            with pytest.raises(PricingBackendError):
                backend.calculate(datetime.now(), datetime.now())

    @pytest.mark.parametrize('initial_date, final_date', [
        (
            datetime(year=2019, month=1, day=1, hour=1, minute=1, second=1),
            datetime(year=2019, month=1, day=1, hour=1, minute=1, second=10)
        ),
        (
            datetime(year=2019, month=2, day=10, hour=10, minute=1, second=1),
            datetime(year=2019, month=2, day=10, hour=10, minute=1, second=55)
        ),
        (
            datetime(year=2019, month=1, day=1, hour=22, minute=1, second=1),
            datetime(year=2019, month=1, day=1, hour=22, minute=1, second=13)
        ),
    ])
    def test_should_return_standard_tariff_without_one_completed_minute(
        self,
        backend,
        initial_date,
        final_date
    ):
        assert backend.calculate(
            initial=initial_date,
            final=final_date
        ) == backend.standard_tariff

    @pytest.mark.parametrize('initial_date, final_date', [
        (
            datetime(year=2019, month=1, day=1, hour=12, minute=35, second=1),
            datetime(year=2019, month=1, day=1, hour=12, minute=36, second=10)
        ),
        (
            datetime(year=2019, month=2, day=10, hour=10, minute=1, second=1),
            datetime(year=2019, month=2, day=10, hour=10, minute=2, second=55)
        )
    ])
    def test_should_return_standard_tariff_plus_call_charge_with_one_completed_minute(  # noqa
        self,
        backend,
        initial_date,
        final_date
    ):
        assert backend.calculate(
            initial=initial_date,
            final=final_date
        ) == (backend.standard_tariff + backend.standard_call_charge)

    @pytest.mark.parametrize('initial_date, final_date, expected', [
        (
            datetime(year=2018, month=12, day=31, hour=20, minute=0, second=0),
            datetime(year=2019, month=1, day=1, hour=1, minute=0, second=0),
            round(Decimal((119 * 0.09) + 0.36), 2)
        ),
        (
            datetime(year=2018, month=12, day=31, hour=20, minute=0, second=0),
            datetime(year=2019, month=1, day=1, hour=9, minute=0, second=0),
            round(Decimal((5 * 60 * 0.09) + 0.36), 2)
        ),
    ])
    def test_should_return_expected_tariff_year_by_year(
        self,
        backend,
        initial_date,
        final_date,
        expected
    ):
        assert backend.calculate(
            initial=initial_date,
            final=final_date
        ) == expected

    def test_should_return_only_expected_tariff(self, backend):
        initial_date = datetime(
            year=2019, month=1, day=1, hour=22, minute=0, second=0
        )
        final_date = datetime(
            year=2019, month=1, day=1, hour=23, minute=0, second=0
        )
        assert backend.calculate(
            initial=initial_date,
            final=final_date
        ) == Decimal('0.36')

    def test_should_return_expected_charge_to_only_ten_minutes_charged(
        self,
        backend
    ):
        initial_date = datetime(
            year=2019, month=12, day=12, hour=4, minute=57, second=13
        )
        final_date = datetime(
            year=2019, month=12, day=12, hour=6, minute=10, second=56
        )
        assert backend.calculate(
            initial=initial_date,
            final=final_date
        ) == Decimal('1.26')
