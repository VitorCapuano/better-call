import logging
from datetime import datetime
from decimal import Decimal

from dateutil.rrule import MINUTELY, rrule
from ramos.mixins import ThreadSafeCreateMixin
from simple_settings import settings

from bettercall.backends.pricing.backend import (
    PricingBackend,
    PricingBackendError
)
from bettercall.settings import constants

logger = logging.getLogger(__name__)


class DefaultPricingBackend(ThreadSafeCreateMixin, PricingBackend):
    id = 'default_pricing'

    @property
    def standard_tariff(self):
        # TODO: Improve to get from organization
        return settings.DEFAULT_PRICING_TARIFF

    @property
    def standard_call_charge(self):
        # TODO: Improve to get from organization
        return settings.DEFAULT_PRICING_CALL_CHARGE

    @property
    def charge_range(self):
        return range(6, 22)

    def calculate(self, initial: datetime, final: datetime):
        logger.info(f'Received calculate from {initial} to {final}.')
        try:
            diference_date = self.__get_diference_date(initial, final)
            all_minutes = self.__seconds_to_minutes(
                seconds=diference_date.total_seconds()
            )

            free_minutes = self._calculate_discount_call_charge(initial, final)
            minutes_to_charge = all_minutes - free_minutes

            logger.info(
                f'Call minutes: {all_minutes}. Free minutes: {free_minutes}. '
                f'Minutes to charge: {minutes_to_charge}. '
                f'Standard tariff: {self.standard_tariff}. '
                f'Standard call charge: {self.standard_call_charge}.'
            )

            return (
                Decimal(minutes_to_charge * self.standard_call_charge) +
                self.standard_tariff
            )

        except Exception as ex:
            logger.error(
                f'Error on calculate from {initial} to {final}. '
                f'Exception: {ex}.'
            )
            raise PricingBackendError() from ex

    def _calculate_discount_call_charge(
        self,
        initial: datetime,
        final: datetime
    ):
        logger.info(f'Received calculate to charge from {initial} to {final}.')

        diference_date = self.__get_diference_date(initial, final)
        all_minutes = self.__seconds_to_minutes(
            seconds=diference_date.total_seconds()
        )

        if diference_date.total_seconds() < 60:
            logger.info(
                f'Returning free charge: 0min, '
                f'from {initial} to {final}'
            )
            return Decimal('0.00')

        rule = self._get_rule_to_free_charge(initial=initial, final=final)
        rule = len(list(rule)) - 1

        if rule == -1:
            # TODO: Normalize all to seconds
            rule = 0

        free_minutes = all_minutes - rule

        logger.info(
            f'Returning free charge: {free_minutes}min, '
            f'from {initial} to {final}'
        )
        return free_minutes

    def _get_rule_to_free_charge(self, initial: datetime, final: datetime):
        return rrule(
            freq=MINUTELY,
            dtstart=initial,
            until=final,
            byhour=self.charge_range
        )

    def __get_diference_date(
        self,
        initial: datetime,
        final: datetime
    ) -> datetime:
        return final - initial

    def __seconds_to_minutes(self, seconds: int) -> int:
        return int(seconds / constants.MINUTES)

    def __hour_to_minutes(self, hours: int) -> int:
        return hours * constants.MINUTES
