from datetime import datetime
from decimal import Decimal

import pytest

from bettercall.backends.pools.pricing import PricingBackendPool
from bettercall.extensions.fake.backend.pricing import (
    FakePricingBackend,
    FakePricingErrorBackend,
    PricingBackendError
)


@pytest.fixture
def initial_date():
    return datetime.now()


@pytest.fixture
def final_date():
    return datetime.now()


class TestFakePricingBackend:

    @pytest.fixture
    def backend(self):
        return FakePricingBackend.create()

    def test_should_get_backend_from_pool(self):
        assert isinstance(
            PricingBackendPool.get(FakePricingBackend.id), FakePricingBackend
        )

    def test_should_return_all_tariffs_with_decimal_instance(
        self,
        backend,
        initial_date,
        final_date
    ):
        assert isinstance(backend.standard_tariff, Decimal)
        assert isinstance(backend.standard_call_charge, Decimal)
        assert isinstance(backend.calculate(initial_date, final_date), Decimal)


class TestFakePricingErrorBackend:

    @pytest.fixture
    def backend(self):
        return FakePricingErrorBackend.create()

    def test_should_get_backend_from_pool(self):
        assert isinstance(
            PricingBackendPool.get(FakePricingErrorBackend.id),
            FakePricingErrorBackend
        )

    def test_should_raise_exception_to_all_tariffs(
        self,
        backend,
        initial_date,
        final_date
    ):
        with pytest.raises(PricingBackendError):
            backend.standard_tariff
            backend.standard_call_charge
            backend.charge_range
            backend.calculate(initial_date, final_date)
