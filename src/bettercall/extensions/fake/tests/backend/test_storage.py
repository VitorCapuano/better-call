import pytest

from bettercall.backends.pools.storage import StorageBackendPool
from bettercall.backends.storage.backend import StorageBackendError
from bettercall.extensions.fake.backend.storage import (
    FakeStorageBackend,
    FakeStorageBackendError
)


class TestStorageBackend:

    @pytest.fixture
    def backend(self):
        return FakeStorageBackend.create()

    def test_should_get_backend_from_pool(self):
        assert isinstance(
            StorageBackendPool.get(FakeStorageBackend.id),
            FakeStorageBackend
        )

    def test_should_call_save(self, backend):
        assert backend.save


class TestFakStorageErrorBackend:

    @pytest.fixture
    def backend(self):
        return FakeStorageBackendError.create()

    def test_should_get_backend_from_pool(self):
        assert isinstance(
            StorageBackendPool.get(FakeStorageBackendError.id),
            FakeStorageBackendError
        )

    async def test_should_raise_exception_to_save(self, backend):
        with pytest.raises(StorageBackendError):
            await backend.save(1, 'ok')
