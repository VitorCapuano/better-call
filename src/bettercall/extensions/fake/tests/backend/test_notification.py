import pytest

from bettercall.backends.pools.notification import NotificationBackendPool
from bettercall.extensions.fake.backend.notification import (
    FakeNotificationBackend,
    FakeNotificationErrorBackend,
    NotificationBackendError
)


class TestFakeNotificationBackend:

    @pytest.fixture
    def backend(self):
        return FakeNotificationBackend.create()

    def test_should_get_backend_from_pool(self):
        assert isinstance(
            NotificationBackendPool.get(FakeNotificationBackend.id),
            FakeNotificationBackend
        )

    def test_should_call_notify_phonecall_bill(self, backend):
        assert backend.notify_phonecall_bill


class TestFakeNotificationErrorBackend:

    @pytest.fixture
    def backend(self):
        return FakeNotificationErrorBackend.create()

    def test_should_get_backend_from_pool(self):
        assert isinstance(
            NotificationBackendPool.get(FakeNotificationErrorBackend.id),
            FakeNotificationErrorBackend
        )

    async def test_should_raise_exception_on_call_notify_phonecall_bill(
        self,
        backend
    ):
        with pytest.raises(NotificationBackendError):
            await backend.notify_phonecall_bill(signed_url='signed_url')
