from ramos.mixins import ThreadSafeCreateMixin

from bettercall.backends.notification.backend import (
    NotificationBackend,
    NotificationBackendError
)


class FakeNotificationBackend(ThreadSafeCreateMixin, NotificationBackend):
    id = 'fake'

    @classmethod
    async def notify_phonecall_bill(cls, signed_url) -> None:
        pass


class FakeNotificationErrorBackend(ThreadSafeCreateMixin, NotificationBackend):
    id = 'fake_error'

    @classmethod
    async def notify_phonecall_bill(cls, signed_url) -> None:
        raise NotificationBackendError('Error.')
