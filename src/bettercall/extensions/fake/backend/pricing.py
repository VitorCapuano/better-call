from datetime import datetime
from decimal import Decimal

from ramos.mixins import ThreadSafeCreateMixin

from bettercall.backends.pricing.backend import (
    PricingBackend,
    PricingBackendError
)
from bettercall.settings import constants


class FakePricingBackend(ThreadSafeCreateMixin, PricingBackend):
    id = 'fake'

    @property
    def standard_tariff(self):
        return Decimal('1.00')

    @property
    def standard_call_charge(self):
        return Decimal('2.00')

    @property
    def charge_range(self):
        return range(0, 1)

    def calculate(self, initial: datetime, final: datetime):
        min_difference = 121 * constants.MINUTES

        total_discount = min_difference - self._calculate_discount_call_charge(
            initial,
            final
        )

        return Decimal(total_discount * self.standard_call_charge)

    def _calculate_discount_call_charge(
        self,
        initial: datetime,
        final: datetime
    ):
        return 1


class FakePricingErrorBackend(ThreadSafeCreateMixin, PricingBackend):
    id = 'fake_error'

    @property
    def standard_tariff(self):
        raise PricingBackendError('Error.')

    @property
    def standard_call_charge(self):
        raise PricingBackendError('Error.')

    @property
    def charge_range(self):
        raise PricingBackendError('Error.')

    def calculate(self, initial: datetime, final: datetime):
        raise PricingBackendError('Error.')

    def _calculate_discount_call_charge(
        self,
        initial: datetime,
        final: datetime
    ):
        raise PricingBackendError('Error.')
