import logging

from ramos.mixins import SingletonCreateMixin

from bettercall.backends.storage.backend import (
    StorageBackend,
    StorageBackendError
)

logger = logging.getLogger(__name__)


class FakeStorageBackend(SingletonCreateMixin, StorageBackend):
    id = 'fake'
    name = 'Fake storage'

    async def save(self, data, filename):
        return 'one-signed-url'


class FakeStorageBackendError(SingletonCreateMixin, StorageBackend):
    id = 'fake_error'

    async def save(self, data, filename) -> None:
        raise StorageBackendError('Error.')
