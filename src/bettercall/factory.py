import aiohttp_jinja2
import jinja2
from aiohttp import web
from simple_settings import settings

from bettercall.middlewares.exception_handler import (
    exception_handler_middleware
)
from bettercall.middlewares.version import version_middleware

from .routes import setup_routes


def build_app():
    app = web.Application(
        middlewares=[version_middleware, exception_handler_middleware]
    )
    setup_routes(app)

    aiohttp_jinja2.setup(
        app,
        loader=jinja2.FileSystemLoader(settings.PROJECT_PATH)
    )

    app['static_root_url'] = '/static/'

    return app
