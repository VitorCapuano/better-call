import asyncio

import pytest

from bettercall import app as _app


@pytest.fixture(scope='session')
def loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture
def app():
    return _app


@pytest.fixture(autouse=True)
def client(aiohttp_client, loop, app):
    return loop.run_until_complete(aiohttp_client(app))
