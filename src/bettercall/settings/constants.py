# flake8: noqa

# DATE
DATE_MONTH_OUTPUT = '%m/%Y'
DATE_FORMAT_OUTPUT = '%d/%m/%Y'
DATETIME_FORMAT_OUTPUT = '%d/%m/%Y %H:%M:%S'

# TIME
SECONDS = 1
MINUTES = SECONDS * 60
HOURS = MINUTES * 60
DAYS = HOURS * 24

# STORAGE
BYTES = 1
KILO_BYTES = 1024 * BYTES
MEGA_BYTES = 1024 * KILO_BYTES

# NOTIFICATION BACKEND
FAKE_NOTIFICATION = 'bettercall.extensions.fake.backend.notification.FakeNotificationBackend'  # noqa
FAKE_NOTIFICATION_ERROR = 'bettercall.extensions.fake.backend.notification.FakeNotificationErrorBackend'  # noqa

# PRICING BACKEND
DEFAULT_PRICING = 'bettercall.extensions.default_pricing.backend.DefaultPricingBackend'  # noqa
FAKE_PRICING = 'bettercall.extensions.fake.backend.pricing.FakePricingBackend'  # noqa
FAKE_PRICING_ERROR = 'bettercall.extensions.fake.backend.pricing.FakePricingErrorBackend'  # noqa

# STORAGE BACKEND
FAKE_STORAGE = 'bettercall.extensions.fake.backend.storage.FakeStorageBackend'  # noqa
FAKE_STORAGE_ERROR = 'bettercall.extensions.fake.backend.storage.FakeStorageBackendError'  # noqa
