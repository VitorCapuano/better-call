import os
from decimal import Decimal
from distutils.util import strtobool

import dj_database_url
import redis

from . import constants

SIMPLE_SETTINGS = {
    'CONFIGURE_LOGGING': True
}

PROJECT_PATH = os.path.dirname(os.path.dirname(__file__))
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = bool(
    strtobool(os.getenv('DEBUG', 'True'))
)

# 'mysql://user:pass@localhost/mydatabase'
DATABASES_DEFAULT_URI = os.environ.get(
    'DATABASES_DEFAULT_URI',
    'sqlite:///:memory:'
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(name)s %(module)s %(process)d %(thread)d %(message)s'  # noqa
        },
        'simple': {
            'format': '%(levelname)s %(asctime)s %(name)s %(message)s'  # noqa
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
            'level': 'INFO',
            'filters': []
        },
        'logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, '..', '..', 'bettercall.log'),
            'maxBytes': 10 * constants.MEGA_BYTES,
            'backupCount': 2,
            'formatter': 'verbose',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': True,
        },
        'bettercall': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False,
        },
        'asyncio': {
            'level': 'WARNING',
            'propagate': True,
        },
    }
}


# redis://{{url}}:{{port}}
redis_url = os.getenv('REDIS_URL', 'redis://127.0.0.1:6379')
CONN = redis.from_url(redis_url)

ALLOWED_HOSTS = ['*']
USE_TZ = True
TIME_ZONE = 'UTC'
STATIC_URL = '/static/'
ROOT_URLCONF = 'bettercall.django_urls'

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'whitenoise.runserver_nostatic',
    'django.contrib.staticfiles',

    'bettercall.phonecall',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

SECRET_KEY = os.getenv('SECRET_KEY', 'fake')
DATABASES = {
    'default': dj_database_url.parse(DATABASES_DEFAULT_URI)
}

DEFAULT_PRICING_BACKEND = os.getenv('DEFAULT_PRICING_BACKEND', 'fake')
DEFAULT_PRICING_TARIFF = Decimal(
    os.getenv('DEFAULT_PRICING_TARIFF', '0.36')
)
DEFAULT_PRICING_CALL_CHARGE = Decimal(
    os.getenv('DEFAULT_PRICING_CALL_CHARGE', '0.09')
)

DEFAULT_NOTIFICATION_BACKEND = os.getenv(
    'DEFAULT_NOTIFICATION_BACKEND', 'fake'
)
DEFAULT_STORAGE_BACKEND = os.getenv('DEFAULT_STORAGE_BACKEND', 'fake')

IS_ENABLE_TO_ENQUEUE_REPORT_BILL = bool(
    strtobool(os.getenv('IS_ENABLE_TO_ENQUEUE_REPORT_BILL', 'False'))
)

ACTIVE_NOTIFICATION_BACKENDS = [
    constants.FAKE_NOTIFICATION
]

PAGINATION = {
    'phonecall': {
        'page_size': int(os.getenv('PAGINATION_PHONECALL_PAGE_SIZE', 100))
    }
}

POOL_OF_RAMOS = {
    'notification': [
        constants.FAKE_NOTIFICATION,
        constants.FAKE_NOTIFICATION_ERROR,
    ],
    'pricing': [
        constants.DEFAULT_PRICING,
        constants.FAKE_PRICING,
        constants.FAKE_PRICING_ERROR,
    ],
    'storage': [
        constants.FAKE_STORAGE,
        constants.FAKE_STORAGE_ERROR,
    ]
}
