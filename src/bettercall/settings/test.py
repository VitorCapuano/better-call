from fakeredis import FakeStrictRedis

from .base import *  # noqa

LOGGING['loggers']['']['handlers'] = ['console', 'logfile']
LOGGING['loggers']['bettercall']['handlers'] = ['console', 'logfile']

CONN = FakeStrictRedis()
