from .base import *  # noqa

DATABASES_DEFAULT_URI = os.environ.get(
    'DATABASES_DEFAULT_URI',
    'sqlite:////tmp/bettercall.db'
)

DATABASES = {
    'default': dj_database_url.parse(DATABASES_DEFAULT_URI)
}
