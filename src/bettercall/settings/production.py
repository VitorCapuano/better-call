import os

import dj_database_url

from . import constants
from .base import *  # noqa
from .base import DATABASES_DEFAULT_URI

DATABASES = {
    'default': dj_database_url.parse(
        DATABASES_DEFAULT_URI,
        conn_max_age=int(
            os.environ.get('DATABASES_DEFAULT_CONN_MAX_AGE', '4')
        ) * constants.HOURS
    )
}
