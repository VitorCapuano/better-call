from enum import Enum


class RecordCallType(Enum):
    START = 'call_started'
    END = 'call_ended'
