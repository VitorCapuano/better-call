from .views import PhoneCallGetView, PhoneCallListCreateView

PHONECALL_ROUTES = [
    ('*', r'/phonecall/{call_id}', PhoneCallGetView, 'get-phone-call'),
    ('*', '/phonecall/', PhoneCallListCreateView, 'list-create-phone-call'),
]
