import logging
from datetime import date as _date
from decimal import Decimal
from typing import List

from django.db import models
from simple_settings import settings

from bettercall.backends.pools.pricing import PricingBackendPool
from bettercall.bills.models import PhoneCallBill
from bettercall.mixins.models import ProtectedRecordTypeMixin
from bettercall.phonecall.exceptions import (
    AlreadyExistsStartCall,
    InvalidTimestamp,
    NonExistentStartCall
)
from bettercall.settings.constants import DATETIME_FORMAT_OUTPUT

from .constants import RecordCallType

logger = logging.getLogger(__name__)


class CallerIdManager(models.Manager):

    def bills(self, date: _date, source: str):
        return super(CallerIdManager, self).get_queryset().filter(
            timestamp__year=date.year,
            timestamp__month=date.month,
            end_call__timestamp__year=date.year,
            end_call__timestamp__month=date.month,
            source=source
        )


class PhoneCall(models.Model, ProtectedRecordTypeMixin):
    _record_type = models.CharField(
        choices=[
            (record_call, record_call.value)
            for record_call in RecordCallType
        ],
        max_length=12,
        db_column='record_type'
    )
    timestamp = models.DateTimeField(null=False)
    call_id = models.UUIDField(null=False)
    source = models.CharField(max_length=13, null=True)
    destination = models.CharField(max_length=13, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    price = models.DecimalField(
        max_digits=11,
        decimal_places=2,
        null=True,
        blank=True
    )

    end_call = models.OneToOneField(
        'self',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    objects = models.Manager()
    caller_id = CallerIdManager()

    class Meta:
        unique_together = (
            ('_record_type', 'call_id')
        )

    def __str__(self):
        return (
            f'Phone call: {self.call_id}, from: {self.source} to: '
            f'{self.destination}.'
        )

    def save(self, *args, **kwargs):
        self.validate()
        super().save(self, *args, **kwargs)

        if self.record_type == RecordCallType.END.value:
            PhoneCall.objects.filter(
                call_id=self.call_id,
                _record_type=RecordCallType.START.value
            ).update(
                end_call=self
            )

    def as_dict(self):
        return {
            'record_type': self.record_type,
            'timestamp': self.timestamp.strftime(DATETIME_FORMAT_OUTPUT),
            'call_id': str(self.call_id),
            'source': self.source,
            'destination': self.destination
        }

    def validate(self):
        """
        Validate phonecall to save
        """
        logger.info(
            f'Creating self with record_type: {self.record_type} to: {self}'
        )
        self.validate_status(
            record_type=self.record_type,
            valid_record_types=(
                RecordCallType.START.value,
                RecordCallType.END.value
            )
        )

        if self.record_type == RecordCallType.START.value:
            try:
                PhoneCall.objects.get(
                    call_id=self.call_id,
                    _record_type=RecordCallType.START.value
                )
                logger.warning(f'Existing start call to: {self}')
                raise AlreadyExistsStartCall()
            except PhoneCall.DoesNotExist:
                logger.info(f'Returning start call to: {self}')
                return

        try:
            start_call = PhoneCall.objects.get(
                call_id=self.call_id,
                _record_type=RecordCallType.START.value
            )
        except PhoneCall.DoesNotExist:
            logger.warning(f'Non existing start call to: {self}')
            raise NonExistentStartCall(f'Non existing start call to: {self}')

        if self.timestamp < PhoneCall.objects.get(
            call_id=self.call_id,
            _record_type=RecordCallType.START.value
        ).timestamp:
            error_message = (
                f'Timestamp {self.timestamp} less than start timestamp '
                f'{start_call.timestamp}.'
            )
            logger.error(error_message)
            raise InvalidTimestamp(error_message)

    @classmethod
    def generate_report_bill(
        cls,
        date: _date,
        source: str
    ) -> List[PhoneCallBill]:
        return [
            PhoneCallBill(
                destination=bill.destination,
                start_time=bill.timestamp,
                end_time=bill.end_call.timestamp,
                price=cls.__get_or_create_price(bill=bill)
            )
            for bill in cls.caller_id.bills(date=date, source=source)
        ]

    @classmethod
    def __get_or_create_price(cls, bill) -> Decimal:
        if bill.price:
            logger.info(
                f'Not possible to calculate. Already exists a defined price: '
                f'{bill.price}'
            )
            return bill.price

        pricing_backend = PricingBackendPool.get(
            settings.DEFAULT_PRICING_BACKEND
        )
        price = pricing_backend.calculate(
            initial=bill.timestamp,
            final=bill.end_call.timestamp
        )
        cls.objects.filter(pk=bill.id).update(price=price)

        return price
