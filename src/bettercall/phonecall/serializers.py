from uuid import uuid4

from marshmallow import Schema, fields, post_load, validate
from marshmallow_oneofschema import OneOfSchema

from bettercall.helpers.date import make_aware_date
from bettercall.helpers.regex import PHONE_MATCH_REGEX
from bettercall.helpers.serializer import HandleErrorMixin

from .constants import RecordCallType
from .models import PhoneCall


class BasePhoneCallSerializer(HandleErrorMixin, Schema):
    record_type = fields.String(
        validate=validate.OneOf(
            choices=(RecordCallType.END.value, RecordCallType.START.value)
        ),
        load_from='type',
        required=True
    )
    timestamp = fields.DateTime(required=True)

    @post_load
    def build_phone_call(self, data: dict) -> PhoneCall:
        data['timestamp'] = make_aware_date(timestamp=data['timestamp'])
        return PhoneCall(**data)


class StartCallSerializer(BasePhoneCallSerializer):
    call_id = fields.UUID(required=False, missing=uuid4())
    source = fields.String(
        validate=validate.Regexp(PHONE_MATCH_REGEX),
        required=True
    )
    destination = fields.String(
        validate=validate.Regexp(PHONE_MATCH_REGEX),
        required=True
    )

    class Meta:
        ordered = True


class EndCallSerializer(BasePhoneCallSerializer):
    call_id = fields.UUID(required=True)

    class Meta:
        ordered = True


class PhoneCallSerializer(OneOfSchema):
    type_field_remove = False
    type_schemas = {
        RecordCallType.START.value: StartCallSerializer,
        RecordCallType.END.value: EndCallSerializer,
    }
