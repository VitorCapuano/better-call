class InvalidRecordType(Exception):
    pass


class InvalidTimestamp(Exception):
    pass


class NonExistentStartCall(Exception):
    pass


class AlreadyExistsStartCall(Exception):
    pass
