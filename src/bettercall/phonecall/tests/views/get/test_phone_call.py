from http import HTTPStatus
from unittest import mock
from uuid import uuid4

import pytest
from django.core.paginator import Paginator
from model_mommy import mommy
from simple_settings import settings


@pytest.mark.django_db
class TestGetPhoneCallList:

    @pytest.fixture
    def url(self, app):
        return app.router['list-create-phone-call'].url_for()

    async def test_should_return_empty_list_to_list_all_with_nothing(
        self,
        client,
        url
    ):
        response = await client.get(path=url)
        json = await response.json()

        assert json['results'] == []

    async def test_should_call_paginator_with_default_settings(
        self,
        client,
        url
    ):
        with mock.patch.object(
            Paginator,
            '__init__'
        ) as mock_paginator_obj:
            await client.get(path=url)

        args, _ = mock_paginator_obj.call_args
        assert args[1] == settings.PAGINATION['phonecall']['page_size']

    async def test_should_return_paginated_response_objects(
        self,
        client,
        url
    ):
        response = await client.get(path=url)
        json = await response.json()

        assert 'count' in json
        assert 'next' in json
        assert 'previous' in json
        assert 'results' in json

    async def test_should_return_paginated_from_database_objects(
        self,
        client,
        url
    ):
        phonecall = mommy.make(
            'phonecall.PhoneCall',
            _record_type='call_started'
        )

        response = await client.get(path=url)
        json = await response.json()

        assert json['results'][0] == phonecall.as_dict()


@pytest.mark.django_db
class TestGetPhoneCallByID:

    @pytest.fixture
    def uuid(self):
        return str(uuid4())

    @pytest.fixture
    def url(self, app, uuid):
        return app.router['get-phone-call'].url_for(call_id=uuid)

    async def test_should_return_not_found_to_random_uuid(
        self,
        client,
        url
    ):
        response = await client.get(path=url)
        assert response.status == HTTPStatus.NOT_FOUND.value

    async def test_should_validate_uuid_on_url(self, app, client):
        url = app.router['get-phone-call'].url_for(call_id='not uuid')

        response = await client.get(path=url)
        json = await response.json()

        assert response.status == HTTPStatus.BAD_REQUEST.value
        assert json == {
            'code': 'invalid_uuid',
            'message': 'Invalid uuid format',
            'details': None
        }

    async def test_should_return_paginated_from_database_objects(
        self,
        client,
        url,
        uuid
    ):
        start_phonecall = mommy.make(
            'phonecall.PhoneCall',
            _record_type='call_started',
            call_id=uuid
        )
        end_phonecall = mommy.make(
            'phonecall.PhoneCall',
            _record_type='call_ended',
            call_id=uuid
        )

        response = await client.get(path=url)
        json = await response.json()

        assert json[0] == start_phonecall.as_dict()
        assert json[1] == end_phonecall.as_dict()
