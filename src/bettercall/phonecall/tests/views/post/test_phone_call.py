from http import HTTPStatus
from unittest import mock

import pytest
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from model_mommy import mommy

from bettercall.phonecall.models import PhoneCall
from bettercall.phonecall.serializers import (
    EndCallSerializer,
    StartCallSerializer
)


@pytest.mark.django_db
class TestCreatePhoneCall:

    @pytest.fixture
    def url(self, app):
        return app.router['list-create-phone-call'].url_for()

    @pytest.fixture
    def invalid_paylod(self):
        return {}

    @pytest.fixture
    def start_call(self, valid_end_call_payload):
        return mommy.make(
            'phonecall.PhoneCall',
            call_id=valid_end_call_payload['call_id'],
            record_type='call_started',
            timestamp=timezone.now() - relativedelta(year=10)
        )

    async def test_should_raise_validation_error(
        self,
        client,
        url,
        invalid_paylod
    ):
        response = await client.post(path=url, json=invalid_paylod)
        data = await response.json()

        assert response.status == HTTPStatus.BAD_REQUEST.value
        assert data['message'] == 'Invalid phone call.'

    async def test_should_return_created_status_code_to_end_call_payload(
        self,
        client,
        url,
        start_call,
        valid_end_call_payload
    ):
        response = await client.post(path=url, json=valid_end_call_payload)
        assert response.status == HTTPStatus.CREATED.value

    async def test_should_return_data_as_dict(
        self,
        client,
        url,
        valid_start_call_payload
    ):
        response = await client.post(path=url, json=valid_start_call_payload)
        payload = await response.json()

        database_object = PhoneCall.objects.first()

        assert payload == database_object.as_dict()

    async def test_should_return_created_to_start_call_payload(
        self,
        client,
        url,
        valid_start_call_payload
    ):
        response = await client.post(path=url, json=valid_start_call_payload)
        assert response.status == HTTPStatus.CREATED.value

    async def test_should_return_bad_request_to_undefined_record_type(
        self,
        client,
        url,
        valid_start_call_payload
    ):
        valid_start_call_payload['type'] = 'unknown'

        response = await client.post(path=url, json=valid_start_call_payload)
        assert response.status == HTTPStatus.BAD_REQUEST.value

    async def test_should_return_integrity_error_to_violate_unique_constraint(
        self,
        client,
        url,
        valid_start_call_payload
    ):
        response = await client.post(path=url, json=valid_start_call_payload)
        assert response.status == HTTPStatus.CREATED.value

        response = await client.post(path=url, json=valid_start_call_payload)
        assert response.status == HTTPStatus.INTERNAL_SERVER_ERROR.value

    async def test_should_return_not_found_when_start_call_not_exists(
        self,
        client,
        url,
        valid_end_call_payload
    ):
        response = await client.post(path=url, json=valid_end_call_payload)
        assert response.status == HTTPStatus.NOT_FOUND.value

    async def test_should_call_end_phonecall_serializer(
        self,
        client,
        valid_end_call_payload,
        url
    ):
        with mock.patch.object(
            EndCallSerializer,
            'load',
            return_value={}
        ) as mock_serializer:

            await client.post(
                path=url,
                json=valid_end_call_payload
            )

        assert mock_serializer.called

    async def test_should_call_start_phonecall_serializer(
        self,
        client,
        valid_start_call_payload,
        url
    ):
        with mock.patch.object(
            StartCallSerializer,
            'load',
            return_value={}
        ) as mock_serializer:

            await client.post(path=url, json=valid_start_call_payload)

        assert mock_serializer.called

    async def test_should_persist_on_database_start_call_from_received_object(
        self,
        client,
        url,
        valid_start_call_payload
    ):
        assert not PhoneCall.objects.first()
        await client.post(path=url, json=valid_start_call_payload)

        database_object = PhoneCall.objects.first()

        assert database_object.record_type == valid_start_call_payload['type']
        assert database_object.source == valid_start_call_payload['source']
        assert database_object.destination == (
            valid_start_call_payload['destination']
        )
