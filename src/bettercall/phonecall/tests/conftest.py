import pytest


@pytest.fixture
def valid_start_call_payload():
    return {
        'type': 'call_started',
        'timestamp': '2014-12-22T03:12:58.019077+00:00',
        'call_id': '8e0261d1-959b-4a19-bff6-ba84c4ed71d9',
        'source': '1122223333',
        'destination': '11922223333',
    }


@pytest.fixture
def valid_end_call_payload():
    return {
        'type': 'call_ended',
        'timestamp': '2014-12-22T03:12:58.019077+00:00',
        'call_id': '8e0261d1-959b-4a19-bff6-ba84c4ed71d9',
    }
