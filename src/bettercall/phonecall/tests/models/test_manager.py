import pytest

from bettercall.helpers.date import make_aware_date
from bettercall.phonecall.models import PhoneCall


@pytest.mark.django_db
class TestCallerIdManager:

    def test_should_return_only_one_line_to_two_records_with_same_call_id(
        self,
        call_start,
        call_end,
        final_date,
    ):
        queryset = PhoneCall.caller_id.bills(
            date=final_date,
            source=call_start.source
        )
        assert len(queryset) == 1

    def test_should_return_expected_fields_from_database(
        self,
        call_start,
        call_end,
        initial_date,
        final_date,
        destination
    ):
        bills = PhoneCall.caller_id.bills(
            date=final_date,
            source=call_start.source
        )

        assert bills[0].destination == destination
        assert bills[0].timestamp == make_aware_date(initial_date)
        assert bills[0].end_call.timestamp == make_aware_date(final_date)
