from decimal import Decimal
from unittest import mock
from uuid import uuid4

import pytest
from model_mommy import mommy

from bettercall.bills.models import PhoneCallBill
from bettercall.helpers.date import make_aware_date
from bettercall.phonecall.models import PhoneCall


@pytest.mark.django_db
class TestReport:

    @pytest.fixture
    def create_unmatch_phonebills_by_source(
        self,
        initial_date,
        destination,
        source
    ):
        uuid = uuid4()
        started = mommy.make(
            'phonecall.PhoneCall',
            _record_type='call_started',
            call_id=uuid,
            timestamp=make_aware_date(initial_date),
            destination=destination,
            source=source + '1'
        )
        ended = mommy.make(
            'phonecall.PhoneCall',
            _record_type='call_ended',
            call_id=uuid,
            timestamp=make_aware_date(initial_date),
            destination=destination,
            source=source + '1'
        )
        return started, ended

    @pytest.fixture
    def mock_calculate_backend(self):
        with mock.patch(
            'bettercall.phonecall.models.PricingBackendPool'
        ) as mock_backend_pool:
            mock_backend = mock_backend_pool.get.return_value
            yield mock_backend.calculate

    def test_should_return_an_empty_list_to_nothing_on_phonecall_database(
        self,
        final_date,
        source
    ):
        assert PhoneCall.generate_report_bill(
            date=final_date,
            source=source
        ) == []

    def test_should_filter_by_source(
        self,
        call_start,
        call_end,
        final_date,
        source,
        create_unmatch_phonebills_by_source
    ):
        different_start_call, different_end_call = (
            create_unmatch_phonebills_by_source
        )

        assert len(
            PhoneCall.generate_report_bill(
                date=final_date,
                source=source
            )
        ) == 1

        assert call_start.source != different_start_call.source
        assert call_end.source != different_end_call.source

    def test_should_return_an_instance_to_report_call_model(
        self,
        call_start,
        call_end,
        final_date,
        source
    ):
        for bill in PhoneCall.generate_report_bill(
            date=final_date,
            source=source
        ):
            assert isinstance(bill, PhoneCallBill)

    def test_should_calculate_price_according_as_pricing_backend(
        self,
        call_start,
        call_end,
        final_date,
        source,
        mock_calculate_backend
    ):
        expected_calculate = Decimal('1.00')
        mock_calculate_backend.return_value = expected_calculate

        bill = PhoneCall.generate_report_bill(
            date=final_date,
            source=source
        )
        assert bill[0].price == expected_calculate

    def test_should_return_expected_fields_according_to_model(
        self,
        call_start,
        call_end,
        final_date,
        source
    ):
        bill = PhoneCall.generate_report_bill(
            date=final_date,
            source=source
        )

        assert bill[0].start_time == make_aware_date(call_start.timestamp)
        assert bill[0].end_time == make_aware_date(call_end.timestamp)
        assert bill[0].destination == call_start.destination
        assert bill[0].destination == call_end.destination

    def test_should_raise_exception_when_pricing_backend_fails(
        self,
        call_start,
        call_end,
        final_date,
        source,
        mock_calculate_backend
    ):
        mock_calculate_backend.side_effect = Exception

        with pytest.raises(Exception):
            PhoneCall.generate_report_bill(
                date=final_date,
                source=source
            )

    def test_should_not_calculate_when_price_is_not_null(
        self,
        mock_calculate_backend,
        call_start,
        call_end,
        final_date,
        source
    ):
        PhoneCall.objects.filter(pk=call_start.pk).update(price=1.00)

        call_start.refresh_from_db()
        assert call_start.price

        PhoneCall.generate_report_bill(
            date=final_date,
            source=source
        )
        assert not mock_calculate_backend.called

    def test_should_calculate_price_when_price_is_null(
        self,
        call_start,
        call_end,
        final_date,
        source,
        mock_calculate_backend
    ):
        expected_value = Decimal('1.00')
        mock_calculate_backend.return_value = expected_value

        assert not call_start.price
        assert not call_end.price

        PhoneCall.generate_report_bill(
            date=final_date,
            source=source
        )
        call_start.refresh_from_db()

        assert call_start.price == expected_value
        assert mock_calculate_backend.called
