import pytest
from dateutil.relativedelta import relativedelta
from model_mommy import mommy

from bettercall.phonecall.exceptions import (
    AlreadyExistsStartCall,
    InvalidRecordType,
    InvalidTimestamp,
    NonExistentStartCall
)


@pytest.mark.django_db
class TestValidate:

    @pytest.fixture
    def phonecall(self):
        return mommy.prepare('phonecall.PhoneCall')

    def test_should_return_validate_record_type_error(self, phonecall):
        phonecall.record_type = 'unknown'
        with pytest.raises(InvalidRecordType):
            phonecall.validate()

    def test_should_return_validate_non_existent_error(self, phonecall):
        phonecall.record_type = 'call_ended'
        with pytest.raises(NonExistentStartCall):
            phonecall.validate()

    def test_should_return_timestamp_error(self, call_start, phonecall):
        phonecall.record_type = 'call_ended'
        phonecall.call_id = call_start.call_id
        phonecall.timestamp = call_start.timestamp - relativedelta(days=1)
        with pytest.raises(InvalidTimestamp):
            phonecall.validate()

    def test_should_return_already_exists_start_call_error(self, call_start):
        with pytest.raises(AlreadyExistsStartCall):
            call_start.validate()

    def test_should_return_to_valid_start_call(self, phonecall):
        try:
            phonecall.record_type = 'call_started'
            phonecall.validate()
        except Exception as e:
            pytest.fail(f'It should not have raised: {e}')
