from datetime import datetime
from uuid import uuid4

import pytest
from model_mommy import mommy

from bettercall.helpers.date import make_aware_date


@pytest.fixture
def uuid():
    return uuid4()


@pytest.fixture
def initial_date():
    return datetime(year=2019, month=7, day=23, hour=2, minute=1)


@pytest.fixture
def final_date():
    return datetime(year=2019, month=7, day=23, hour=3, minute=1)


@pytest.fixture
def destination():
    return "1199999999"


@pytest.fixture
def source():
    return "1199999991"


@pytest.fixture
def call_start(uuid, initial_date, destination, source):
    return mommy.make(
        'phonecall.PhoneCall',
        _record_type='call_started',
        call_id=uuid,
        timestamp=make_aware_date(initial_date),
        destination=destination,
        source=source
    )


@pytest.fixture
def call_end(uuid, final_date, destination, source):
    return mommy.make(
        'phonecall.PhoneCall',
        _record_type='call_ended',
        call_id=uuid,
        timestamp=make_aware_date(final_date),
        destination=destination,
        source=source
    )
