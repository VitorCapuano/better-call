from datetime import datetime
from uuid import UUID

import pytest

from bettercall.exceptions.api import ValidationError
from bettercall.phonecall.models import PhoneCall
from bettercall.phonecall.serializers import StartCallSerializer


class TestAnticipationCallbackSerializer:

    def test_should_return_an_instance_to_phone_call_model(
        self,
        valid_start_call_payload
    ):
        data = StartCallSerializer(strict=True).load(
            valid_start_call_payload
        ).data

        assert isinstance(data, PhoneCall)

    def test_should_serialize_all_fields(self, valid_start_call_payload):
        data = StartCallSerializer(strict=True).load(
            valid_start_call_payload
        ).data

        assert data.record_type == valid_start_call_payload['type']
        assert data.destination == valid_start_call_payload['destination']
        assert data.source == valid_start_call_payload['source']

        assert isinstance(data.call_id, UUID)
        assert isinstance(data.timestamp, datetime)

    def test_should_generate_call_uuid_without_them(
        self,
        valid_start_call_payload
    ):
        del valid_start_call_payload['call_id']

        data = StartCallSerializer(strict=True).load(
            valid_start_call_payload
        ).data

        assert isinstance(data.call_id, UUID)

    def test_should_raise_validation_error_to_unknown_type(
        self,
        valid_start_call_payload
    ):
        valid_start_call_payload['type'] = 'invalid_choice'

        with pytest.raises(ValidationError):
            StartCallSerializer(strict=True).load(
                valid_start_call_payload
            ).data

    @pytest.mark.parametrize('invalid_phone', [
        '99',
        '99',
        '999',
        '9999',
        '99999',
        '199999',
        '1199999',
        '11999999',
    ])
    def test_should_raise_validation_error_to_invalid_source_phone(
        self,
        valid_start_call_payload,
        invalid_phone
    ):
        valid_start_call_payload['source'] = invalid_phone

        with pytest.raises(ValidationError):
            StartCallSerializer(strict=True).load(
                valid_start_call_payload
            ).data

    @pytest.mark.parametrize('invalid_phone', [
        '99',
        '99',
        '999',
        '9999',
        '99999',
        '199999',
        '1199999',
        '11999999',
    ])
    def test_should_raise_validation_error_to_invalid_destination_phone(
        self,
        valid_start_call_payload,
        invalid_phone
    ):
        valid_start_call_payload['destination'] = invalid_phone

        with pytest.raises(ValidationError):
            StartCallSerializer(strict=True).load(
                valid_start_call_payload
            ).data

    @pytest.mark.parametrize('remove_field', [
        'type',
        'timestamp',
        'source',
        'destination'
    ])
    def test_should_raise_exception_without_required_fields(
        self,
        valid_start_call_payload,
        remove_field
    ):
        del valid_start_call_payload[remove_field]

        with pytest.raises(ValidationError):
            StartCallSerializer(strict=True).load(
                valid_start_call_payload
            ).data
