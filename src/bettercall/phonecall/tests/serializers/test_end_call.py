from datetime import datetime
from uuid import UUID

import pytest

from bettercall.exceptions.api import ValidationError
from bettercall.phonecall.models import PhoneCall
from bettercall.phonecall.serializers import EndCallSerializer


class TestAnticipationCallbackSerializer:

    def test_should_return_an_instance_to_phone_call_model(
        self,
        valid_end_call_payload
    ):
        data = EndCallSerializer(strict=True).load(
            valid_end_call_payload
        ).data

        assert isinstance(data, PhoneCall)

    def test_should_serialize_all_fields(self, valid_end_call_payload):
        data = EndCallSerializer(strict=True).load(
            valid_end_call_payload
        ).data

        assert data.record_type == valid_end_call_payload['type']

        assert isinstance(data.call_id, UUID)
        assert isinstance(data.timestamp, datetime)

    def test_should_raise_validation_error_to_unknown_type(
        self,
        valid_end_call_payload
    ):
        valid_end_call_payload['type'] = 'invalid_choice'

        with pytest.raises(ValidationError):
            EndCallSerializer(strict=True).load(
                valid_end_call_payload
            ).data

    @pytest.mark.parametrize('remove_field', [
        'type',
        'timestamp',
        'call_id'
    ])
    def test_should_raise_exception_without_required_fields(
        self,
        valid_end_call_payload,
        remove_field
    ):
        del valid_end_call_payload[remove_field]

        with pytest.raises(ValidationError):
            EndCallSerializer(strict=True).load(
                valid_end_call_payload
            ).data
