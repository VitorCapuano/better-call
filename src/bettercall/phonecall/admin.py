from django.contrib import admin

from .models import PhoneCall


class PhoneCallAdmin(admin.ModelAdmin):
    pass


admin.site.register(PhoneCall, PhoneCallAdmin)
