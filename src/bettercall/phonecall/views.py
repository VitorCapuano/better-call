import logging
from http import HTTPStatus

from aiohttp import web
from django.core.paginator import Paginator
from django.db import IntegrityError
from marshmallow.exceptions import ValidationError as SchemaValidationError
from simple_settings import settings

from bettercall.exceptions.api import APIException, NotFound, ValidationError
from bettercall.helpers import uuid
from bettercall.helpers.pagination import build_paginated_response
from bettercall.phonecall.models import PhoneCall

from .exceptions import (
    InvalidRecordType,
    InvalidTimestamp,
    NonExistentStartCall
)
from .serializers import PhoneCallSerializer

logger = logging.getLogger(__name__)


class PhoneCallListCreateView(web.View):

    async def post(self):
        body = await self.request.json()
        try:
            data = PhoneCallSerializer(strict=True).load(body).data
        except SchemaValidationError:
            raise ValidationError(
                code='invalid_record_type',
                message='Invalid phone call.'
            )

        logger.info(f'Create phonecall with data: {data}.')
        try:
            data.save()
        except IntegrityError as e:
            logger.error(f'Error integrity: {e}')
            raise APIException(code='integrity_error')
        except (InvalidTimestamp, InvalidRecordType):
            raise ValidationError()
        except NonExistentStartCall:
            raise NotFound(
                code='non_existent_start_call',
                message='Non Existent start call with call_id referenced'
            )

        return web.json_response(
            data.as_dict(),
            status=HTTPStatus.CREATED.value
        )

    async def get(self):
        logger.info(f'Get list of phonecalls.')

        phone_calls = PhoneCall.objects.all()

        paginator = Paginator(
            phone_calls,
            settings.PAGINATION['phonecall']['page_size']
        )
        page = paginator.get_page(self.request.query.get('page', 1))

        logger.info(f'Get page: {page} from list of phonecalls.')

        return web.json_response(
            build_paginated_response(
                url=self.request.url,
                page=page,
                serialized_data=[
                    phonecall.as_dict()
                    for phonecall in page.object_list
                ]
            ),
            status=HTTPStatus.OK.value
        )


class PhoneCallGetView(web.View):

    async def get(self):
        call_id = self.request.match_info.get('call_id')

        logger.info(f'Get phonecall with call_id: {call_id}.')
        try:
            uuid.validate(call_id)
        except uuid.ValidateUUIDError:
            logger.error(f'Error to get call_id: {call_id}. Is not UUID.')
            raise ValidationError(
                code='invalid_uuid',
                message='Invalid uuid format'
            )

        phonecalls = PhoneCall.objects.filter(call_id=call_id)
        if not phonecalls.exists():
            logger.warning(
                'Phone call with call_id: {call_id} does not exist.'
            )
            raise NotFound()

        return web.json_response(
            [
                phonecall.as_dict()
                for phonecall in phonecalls
            ],
            status=HTTPStatus.OK.value
        )
