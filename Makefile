settings=bettercall.settings.development

VERSION := 1.3.1

ifdef SIMPLE_SETTINGS
	settings=$(SIMPLE_SETTINGS)
else
	export SIMPLE_SETTINGS=$(settings)
endif

export PYTHONPATH=$(shell pwd)/src/
export PYTHONDONTWRITEBYTECODE=1
export DJANGO_SETTINGS_MODULE=$(settings)

.PHONY: help

clean:
	@find . -name "*.pyc" | xargs rm -rf
	@find . -name "*.pyo" | xargs rm -rf
	@find . -name "*.log" | xargs rm -rf
	@find . -name "__pycache__" -type d | xargs rm -rf
	@find . -name ".pytest_cache" -type d | xargs rm -rf
	@rm -f .coverage
	@rm -rf htmlcov/
	@rm -f coverage.xml
	@rm -f *.log

flake8:
	@flake8 --show-source .

fix-python-import:
	@isort -rc -y

run:
	@gunicorn bettercall:app --bind localhost:8080 --worker-class aiohttp.worker.GunicornUVLoopWebWorker -e SIMPLE_SETTINGS=bettercall.settings.development

admin:
	gunicorn -b 1.3.1.0 --pythonpath src bettercall.django_wsgi

requirements-dev:
	@pip install -U -r requirements/development.txt

test: clean
	@py.test -x --cov=src/bettercall/ --cov-report=term-missing

test-matching: clean
	@py.test -k $(Q) --pdb -x src/bettercall


test-coverage: clean ## Run entire test suite with coverage
	@py.test -x src/bettercall --cov --cov-report=term-missing --cov-report=xml

shell: ## Run repl
	@echo 'Loading shell with settings = $(settings)'
	@PYTHONSTARTUP=.startup.py ipython

migrate: ## Apply migrations
	@django-admin migrate --no-input

migrations: ## Create migrations
	@django-admin makemigrations

detect-migrations:  ## Detect missing migrations
	@django-admin makemigrations --dry-run --noinput | grep 'No changes detected' -q || (echo 'Missing migration detected!' && exit 1)

run-worker:
	python src/bettercall/worker.py

release-draft: ## Show new release changelog
	towncrier --draft

release-patch: ## Create patch release
	bumpversion patch
	towncrier --yes
	git commit -am 'Update CHANGELOG'

release-minor: ## Create minor release
	bumpversion minor
	towncrier --yes
	git commit -am 'Update CHANGELOG'

release-major: ## Create major release
	bumpversion major
	towncrier --yes
	git commit -am 'Update CHANGELOG'

